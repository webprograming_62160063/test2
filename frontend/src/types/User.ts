type Gender = 'male' | 'female' | 'others'
type Role = 'admin' | 'user'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  gender: Gender // Male, Female, Others
  roles: Role[] // admin, user
  nameBills: string
  price: number
  cash: number
  change: number
  paymentMethod: string
}

export type { Gender, Role, User }